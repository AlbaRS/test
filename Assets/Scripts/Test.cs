﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Test : MonoBehaviour
{

    public int num = 0;

    void Awake()
    {
        Debug.Log("Awake del test "+num);
    }

    private void OnEnable()
    {
        Debug.Log("OnEnable del test " + num);
    }

    private void Start()
    {
        Debug.Log("Start del test " + num);
    }


    // Update is called once per frame
    void Update()
    {
        Debug.Log("Update del test " +num);
    }

    void LateUpdate()
    {
        Debug.LogWarning("LateUpdate del test " +num);
    }
}
